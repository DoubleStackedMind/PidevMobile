/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.gui;

import com.codename1.charts.util.ColorUtil;
import com.codename1.components.ImageViewer;
import com.codename1.ui.Button;
import com.codename1.ui.Container;
import com.codename1.ui.EncodedImage;
import com.codename1.ui.Form;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.URLImage;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.geom.Dimension;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.plaf.Border;
import com.codename1.ui.plaf.UIManager;
import com.codename1.ui.util.Resources;
import com.mycompany.Entite.Produits;
import com.mycompany.Service.AddToCartService;
import com.mycompany.Service.ProduitsService;
import com.mycompany.myapp.MyApplication;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Sami
 */
public class AfficherProduits {

    Form f = new Form();
    private Resources theme;
    private EncodedImage ei;
    private EncodedImage ei1;

    public Form getForm() {
        return f;
    }

    public void setForm(Form f) {
        this.f = f;
    }

    public void AfficherProduits() {
        theme = UIManager.initFirstTheme("/theme");
        AddToCartService atcs = new AddToCartService();
        f.getToolbar().addCommandToOverflowMenu("Panier", null, e -> {
            atcs.DispalyCart();
        });

        try {
            ei = EncodedImage.create("/loading.png");
            ei1 = EncodedImage.create("/loading.png");
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
        ProduitsService ps = new ProduitsService();
        List<Produits> myList = new ArrayList();
        myList.addAll(ps.consulterProduits());
        for (Produits p : myList) {

            Button AddToCart = new Button("ADD TO CART");
            Container PriceTAG = new Container(new BoxLayout(BoxLayout.X_AXIS));
            Container IMG = new Container();
            Container c3 = new Container(new BoxLayout(BoxLayout.Y_AXIS));
            Container c1 = new Container(new BoxLayout(BoxLayout.Y_AXIS));
            Label myLabel = new Label();
            Container c2 = new Container(new BoxLayout(BoxLayout.X_AXIS));
            System.out.println(p.getImage());
            Image img = URLImage.createToStorage(ei, p.getImage(),
                    "http://localhost/PIDEV/web/imagesShop/" + p.getImage(), URLImage.RESIZE_SCALE);

            Container Quan = new Container(new BoxLayout(BoxLayout.X_AXIS));
            Button plus = new Button("+");
            Button moins = new Button("-");
            Label Quantity = new Label();
            Quantity.setText("1");
            plus.addActionListener(e -> {
                if (Integer.parseInt(Quantity.getText()) < p.getQuantite()) {
                    Quantity.setText(String.valueOf(Integer.parseInt(Quantity.getText()) + 1));
                }
            });
            moins.addActionListener(e -> {
                if (Integer.parseInt(Quantity.getText()) > 0) {
                    Quantity.setText(String.valueOf(Integer.parseInt(Quantity.getText()) - 1));
                }
            });
            Quantity.getAllStyles().setBorder(Border.createLineBorder(2, "RED"));

            Label Etat = new Label();
            if (p.getQuantite() > 0) {
                Quan.addAll(moins, Quantity, plus);
                AddToCart.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent evt) {

                        atcs.AddToCart(p, Integer.parseInt(Quantity.getText()));
                        System.out.println("Added");
                    }
                });

            } else if (0 < p.getQuantite() && p.getQuantite() < 10) {
                Quan.addAll(moins, Quantity, plus);
            } else {
                Etat.setText(" N'est pas disponible! ");
                Etat.getAllStyles().setBgColor(ColorUtil.rgb(255, 245, 230));
                Quan.add(Etat);
                AddToCart.setVisible(false);
            }
            IMG.setPreferredSize(new Dimension(100, 100));
            myLabel.setPreferredSize(new Dimension(100, 100));
            img.scale(20, 20);
            ImageViewer iv = new ImageViewer(img);
            myLabel.setIcon(img);
            Label nomProduits = new Label();
            nomProduits.setText(p.getNom());
            IMG.add(iv);
            IMG.setLayout(new BoxLayout(BoxLayout.Y_AXIS));
            iv.getParent().revalidate();
            c2.add(IMG);
            c1.add(nomProduits);
            Label price = new Label();
            price.setText(String.valueOf(p.getPrix()));
            PriceTAG.add(new Label(String.valueOf(p.getPrix())));
            PriceTAG.add(AddToCart);
            c1.add(Quan);
            c1.add(PriceTAG);
            c2.add(c1);
            c3.add(c2);
            f.add(c3);
        }

        f.getToolbar().addCommandToLeftBar("Back", theme.getImage("back-command.png"), e -> {
            MyApplication my = new MyApplication();
            Form f = new Form();
            my.setF(f);
            my.start();
        });

        f.show();

        f.addPullToRefresh(new Runnable() {
            @Override
            public void run() {
                f.removeAll();
                AfficherProduits();
            }
        });
    }

}
