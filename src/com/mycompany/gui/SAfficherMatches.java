/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.gui;

import com.codename1.components.ImageViewer;
import com.codename1.ui.Button;
import com.codename1.ui.Command;
import com.codename1.ui.Container;
import com.codename1.ui.Dialog;
import com.codename1.ui.EncodedImage;
import com.codename1.ui.Form;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.TextField;
import com.codename1.ui.URLImage;
import com.codename1.ui.geom.Dimension;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.plaf.UIManager;
import com.codename1.ui.util.Resources;
import com.mycompany.Entite.Equipe;
import com.mycompany.Entite.Matches;
import com.mycompany.Service.SEquipeService;
import com.mycompany.Service.SMatchesServices;
import com.mycompany.Service.TicketService;
import com.mycompany.myapp.MyApplication;
import java.io.IOException;
import java.util.List;

/**
 *
 * @author Sami
 */
public class SAfficherMatches {

    Form f = new Form("Matches");
    EncodedImage ei;
    private Resources theme;

    public void SAfficherMatches() {
        theme = UIManager.initFirstTheme("/theme");
        TicketService ts = new TicketService();
        SMatchesServices sms = new SMatchesServices();
        List<Matches> myList = sms.AfficherMatches();

        try {
            ei = EncodedImage.create("/loading.png");
        } catch (IOException ex) {
            System.out.println("Input Output Exception : " + ex.getMessage());
        }
        for (Matches m : myList) {
            SEquipeService se = new SEquipeService();
            Container c1 = new Container(new BoxLayout(BoxLayout.X_AXIS));
            Container c2 = new Container(new BoxLayout(BoxLayout.Y_AXIS));
            Container c3 = new Container(new BoxLayout(BoxLayout.Y_AXIS));

            Container IMG = new Container(new BoxLayout(BoxLayout.Y_AXIS));
            Container IMG2 = new Container(new BoxLayout(BoxLayout.Y_AXIS));
            Equipe e = se.findEq(m.getEquipeA());
            System.out.println(e.getImage());
            Image img = URLImage.createToStorage(ei, e.getImage(),
                    "http://localhost/PIDEV/web/imagesShop/" + e.getImage(), URLImage.RESIZE_SCALE);
            img.scale(20, 20);
            ImageViewer iv = new ImageViewer(img);

            IMG.setPreferredSize(new Dimension(100, 100));
            Label nomE = new Label();
            nomE.setText(e.getNom());
            Equipe e2 = se.findEq(m.getEquipeB());
            Image img2 = URLImage.createToStorage(ei, e2.getImage(),
                    "http://localhost/PIDEV/web/imagesShop/" + e2.getImage(), URLImage.RESIZE_SCALE);
            img2.scale(20, 20);
            ImageViewer iv2 = new ImageViewer(img2);
            IMG.setPreferredSize(new Dimension(100, 100));
            IMG2.setPreferredSize(new Dimension(100, 100));
            Label nomE2 = new Label();
            nomE2.setText(e2.getNom());
            IMG.add(iv);
            c1.add(IMG);
            IMG2.add(iv2);
            c1.add(new Label(" VS "));
            c1.add(IMG2);
            Button reserver = new Button("Reserver");
            reserver.addActionListener(x -> {
                TextField tf = new TextField();
                tf.addDataChangedListener((i, ii) -> {
                    if (isValidInput(tf.getText())) {
                        tf.putClientProperty("LastValid", tf.getText());
                    } else {
                        tf.stopEditing();
                        tf.setText((String) tf.getClientProperty("LastValid"));
                        tf.startEditingAsync();
                    }
                });
                Dialog.show(
                        "Quantite",
                        FlowLayout.encloseCenterMiddle(tf),
                        Command.create("Cancel", null, (aActionEvent2) -> {
                        }),
                        Command.create("Valider", null, (aActionEvent2) -> {
                            ts.ReserverTicket(m.getId_match(), 2, Integer.parseInt(tf.getText()));
                            SAfficherTicket sas = new SAfficherTicket();
                            sas.AfficherTicket();
                        }));

            });
            c2.add(c1);
            c3.addAll(c2, reserver);
            f.add(c3);
        }
        f.getToolbar().addCommandToLeftBar("Back", theme.getImage("back-command.png"), e -> {
            MyApplication my = new MyApplication();
            Form f = new Form();
            my.setF(f);
            my.start();
        });
        f.show();
    }

    public boolean isValidInput(String input) {

        if (input.contains("a") || input.contains("b") || input.contains("c")
                || input.contains("d") || input.contains("e") || input.contains("f")
                || input.contains("g") || input.contains("h") || input.contains("i")
                || input.contains("j") || input.contains("k") || input.contains("l")
                || input.contains("m") || input.contains("n") || input.contains("o")
                || input.contains("p") || input.contains("q") || input.contains("r")
                || input.contains("s") || input.contains("t") || input.contains("u")
                || input.contains("v") || input.contains("w") || input.contains("x")
                || input.contains("y") || input.contains("z")) {
            return false;
        } else {
            return true;
        }
    }
}
