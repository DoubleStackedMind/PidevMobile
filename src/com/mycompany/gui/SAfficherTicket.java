/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.gui;

import com.codename1.components.ImageViewer;
import com.codename1.ui.Button;
import com.codename1.ui.Container;
import com.codename1.ui.EncodedImage;
import com.codename1.ui.Form;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.URLImage;
import com.codename1.ui.geom.Dimension;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.plaf.UIManager;
import com.codename1.ui.util.Resources;
import com.mycompany.Entite.Equipe;
import com.mycompany.Entite.Matches;
import com.mycompany.Entite.Tickets;
import com.mycompany.Service.SEquipeService;
import com.mycompany.Service.SMatchesServices;
import com.mycompany.Service.TicketService;
import com.mycompany.myapp.MyApplication;
import java.util.List;

/**
 *
 * @author Sami
 */
public class SAfficherTicket {

    Form f = new Form("Tickets");
    private Resources theme;
    EncodedImage ei;

    public void AfficherTicket() {
        theme = UIManager.initFirstTheme("/theme");
        TicketService ts = new TicketService();
        SMatchesServices sms = new SMatchesServices();
        SEquipeService ses = new SEquipeService();
        List<Tickets> myList = ts.consulterTickets(2);
        Button Vendre = new Button("Vendre");
        Vendre.addActionListener(e -> {
            MarchiNoir mn = new MarchiNoir();
        });
        try {
            ei = EncodedImage.create("/loading.png");
        } catch (Exception e) {
            System.out.println("Input Output Exception : " + e.getMessage());
        }
        for (Tickets t : myList) {
            Matches m = sms.findMatche(t.getIdMatch());
            Equipe A = ses.findEq(m.getEquipeA());
            Equipe B = ses.findEq(m.getEquipeB());

            Container IMG = new Container(new BoxLayout(BoxLayout.Y_AXIS));
            Container IMG2 = new Container(new BoxLayout(BoxLayout.Y_AXIS));
            Label nbrTickets = new Label();
            nbrTickets.setText("Nombre de Tickets: " + String.valueOf(t.getQuantite()));
            Image img = URLImage.createToStorage(ei, A.getImage(),
                    "http://localhost/PIDEV/web/imagesShop/" + A.getImage(), URLImage.RESIZE_SCALE);
            ImageViewer iv = new ImageViewer(img);
            img.scale(20, 20);
            iv.setPreferredSize(new Dimension(50, 50));
            IMG.add(iv);
            Image img2 = URLImage.createToStorage(ei, B.getImage(),
                    "http://localhost/PIDEV/web/imagesShop/" + B.getImage(), URLImage.RESIZE_SCALE);
            ImageViewer iv2 = new ImageViewer(img2);
            img2.scale(20, 20);
            iv2.setPreferredSize(new Dimension(50, 50));
            IMG2.add(iv2);

            Container c1 = new Container(new BoxLayout(BoxLayout.X_AXIS));
            Container c2 = new Container(new BoxLayout(BoxLayout.Y_AXIS));
            c1.addAll(IMG, new Label(" VS "), IMG2);

            c2.addAll(nbrTickets, c1);
            f.add(c2);

        }
        f.add(Vendre);
        f.getToolbar().addCommandToLeftBar("Back", theme.getImage("back-command.png"), e -> {
            MyApplication my = new MyApplication();
            Form f = new Form();
            my.setF(f);
            my.start();
        });
        f.show();
    }

}
