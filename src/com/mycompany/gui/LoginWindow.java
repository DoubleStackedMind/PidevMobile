    /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.gui;

import com.codename1.ui.Button;
import com.codename1.ui.Container;
import com.codename1.ui.Form;
import com.codename1.ui.Label;
import com.codename1.ui.TextField;
import com.codename1.ui.layouts.BoxLayout;

/**
 *
 * @author Sami
 */
public class LoginWindow {
    
    
    Form f;
    Label username;
    Label password;
    TextField tfLogin;
    TextField tfPassword;
    Button login;
    Button reset;
    public static int iduser;

    public LoginWindow() {
        f = new Form("S'autehntifier", new BoxLayout(BoxLayout.Y_AXIS));
        username = new Label("Username");
        tfLogin = new TextField(null, "Username", 10, 0);
        password = new Label("Password");
        tfPassword = new TextField(null, "Password", 10, 0);

        tfPassword.setConstraint(TextField.PASSWORD);

        login = new Button("Login");
        reset = new Button("Annuler");

        Container L1 = new Container(new BoxLayout(BoxLayout.X_AXIS));
        Container L2 = new Container(new BoxLayout(BoxLayout.X_AXIS));
        Container L3 = new Container(new BoxLayout(BoxLayout.X_AXIS));

        L1.add(username);
        L1.add(tfLogin);
        L2.add(password);
        L2.add(tfPassword);
        L3.add(login);
        L3.add(reset);

        f.add(L1);
        f.add(L2);
        f.add(L3);
        f.show();

           login.addActionListener((e) -> {
            
               
              
          });
        
        reset.addActionListener((e) -> {
              tfLogin.setText("");
              tfPassword.setText("");
              
          });

    }

    public Form getF() {
        return f;
    }

    public void setF(Form f) {
        this.f = f;
    }

    public Label getUsername() {
        return username;
    }

    public void setUsername(Label username) {
        this.username = username;
    }

    public Label getPassword() {
        return password;
    }

    public void setPassword(Label password) {
        this.password = password;
    }

    public TextField getTfLogin() {
        return tfLogin;
    }

    public void setTfLogin(TextField tfLogin) {
        this.tfLogin = tfLogin;
    }

    public TextField getTfPassword() {
        return tfPassword;
    }

    public void setTfPassword(TextField tfPassword) {
        this.tfPassword = tfPassword;
    }

    public Button getLogin() {
        return login;
    }

    public void setLogin(Button login) {
        this.login = login;
    }

    public static int getIduser() {
        return iduser;
    }

    public static void setIduser(int iduser) {
        LoginWindow.iduser = iduser;
    }
    
}
