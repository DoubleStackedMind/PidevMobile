/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.gui;

import com.codename1.components.ImageViewer;
import com.codename1.components.SpanLabel;
import com.codename1.social.Login;
import com.codename1.ui.BrowserComponent;
import com.codename1.ui.Button;
import com.codename1.ui.Command;
import com.codename1.ui.Container;
import com.codename1.ui.Dialog;
import com.codename1.ui.EncodedImage;
import com.codename1.ui.Form;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.URLImage;
import com.codename1.ui.geom.Dimension;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.plaf.UIManager;
import com.codename1.ui.util.Resources;
import com.mycompany.Entite.Lignedecommande;
import com.mycompany.Entite.Produits;
import com.mycompany.Service.CommandesServices;
import com.mycompany.Service.ProduitsService;
import com.mycompany.myapp.MyApplication;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Sami
 */
public class AfficherCommandes {

    Form f = new Form("Vos Commandes");
    private Resources theme;
    private EncodedImage ei;

    float Totale2 = 0;
    float Totale = 0;

    public Form getF() {
        return f;
    }

    public void setF(Form f) {
        this.f = f;
    }

    public void AfficherCommandes() {
        theme = UIManager.initFirstTheme("/theme");
        try {
            ei = EncodedImage.create("/loading.png");
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
        CommandesServices cs = new CommandesServices();
        ProduitsService ps = new ProduitsService();
        Map<Integer, List<Lignedecommande>> myMap = cs.DisplayCommandes();
        int num = 0;
        System.out.println("my Map : " + myMap.entrySet());
        for (Integer x : myMap.keySet()) {

            Totale = 0;
            num++;
            Label CommNum = new Label();
            CommNum.setText("Commande N° : " + String.valueOf(num));

            Container CommContainer = new Container(new BoxLayout(BoxLayout.Y_AXIS));
            CommContainer.add(CommNum);
            for (Lignedecommande l : myMap.get(x)) {
                Produits p = ps.findProduits(l.getIdProduit());

                Container IMG = new Container(new BoxLayout(BoxLayout.Y_AXIS));
                Container ProdContainer1 = new Container(new BoxLayout(BoxLayout.Y_AXIS));
                Container ProdContainer2 = new Container(new BoxLayout(BoxLayout.X_AXIS));
                Container ProdContainer4 = new Container(new BoxLayout(BoxLayout.X_AXIS));
                Container ProdContainer3 = new Container(new BoxLayout(BoxLayout.Y_AXIS));
                //  p.setImage(px.substring(px.indexOf("image=Mexico.png")+6, px.indexOf(".png")+4));
                //System.out.println("Image : "+ p.getImage());
                Image img = URLImage.createToStorage(ei, p.getImage(),
                        "http://localhost/PIDEV/web/imagesShop/" + p.getImage(), URLImage.RESIZE_SCALE);
                img.scale(20, 20);
                Totale = Totale + p.getPrix();
                Totale2 = Totale2 + p.getPrix();
                ImageViewer iv = new ImageViewer(img);
                iv.setPreferredSize(new Dimension(50, 50));
                IMG.add(iv);
                IMG.setLayout(new BoxLayout(BoxLayout.Y_AXIS));

                ProdContainer2.add(IMG);

                SpanLabel nomProd = new SpanLabel();
                nomProd.setText(p.getNom());

                Label PrixProd = new Label();
                PrixProd.setText(String.valueOf(p.getPrix()));

                Button SupprimerP = new Button("Supprimer");

                SupprimerP.addActionListener(e -> {
                    cs.RemoveProduct(l.getIdLigne());
                    f.removeAll();
                    AfficherCommandes();
                });

                Container SuppCon = new Container(new BoxLayout(BoxLayout.Y_AXIS));
                SuppCon.add(SupprimerP);

                ProdContainer4.addAll(PrixProd, SuppCon);
                ProdContainer1.addAll(nomProd, ProdContainer4);

                ProdContainer2.add(ProdContainer1);
                ProdContainer3.add(ProdContainer2);
                CommContainer.add(ProdContainer3);
            }
            Button pay = new Button("Payer $" + Totale);
            pay.addActionListener(e -> {
                   Dialog.show(
                        "Paiement",
                        FlowLayout.encloseCenterMiddle(new SpanLabel("Choisir votre Methode de Paiement!")),
                        Command.create("Klick&Pay", null, (aActionEvent2) -> {
                            Form hi = new Form("Payement", new BorderLayout());
                            BrowserComponent browser = new BrowserComponent();

                            browser.setURL("http://localhost/tranche.php?MONTANT=" + Totale + "&NAME=" + "Sami" + "&EMAIL=" + "sami.asfouri@esprit.tn");
                            f.removeAll();
                            Container con = new Container(new BorderLayout(BorderLayout.CENTER_BEHAVIOR_CENTER));
                            
                            f.add(browser);
                            f.show();
                        }),
                        Command.create("PayPal", null, (aActionEvent2) -> {
                Form hi = new Form("PayPal");
                BrowserComponent cmp = new BrowserComponent();
                cmp.setPage("<form id=\"checkout\" method=\"post\" action=\"/checkout\">\n"
                        + "  <div id=\"payment-form\"></div>\n"
                        + "  <input type=\"submit\" value=\"Pay $" + Totale + "\">\n"
                        + "</form>\n"
                        + "\n"
                        + "<script src=\"https://js.braintreegateway.com/js/braintree-2.32.1.min.js\"></script>\n"
                        + "<script>\n"
                        + "// We generated a client token for you so you can test out this code\n"
                        + "// immediately. In a production-ready integration, you will need to\n"
                        + "// generate a client token on your server (see section below).\n"
                        + "var clientToken = \"eyJ2ZXJzaW9uIjoyLCJhdXRob3JpemF0aW9uRmluZ2VycHJpbnQiOiI4OTQyNmY5OTUwNjkzM2EwMWYzZmYyMDlkZWY5NmQ4ODA1ODdhMzZhNTkzYjEzYTc4NWJmODIwMjM0YjdhMTZkfGNyZWF0ZWRfYXQ9MjAxOC0wNS0wMlQwMzo1NzoyNi4wMTMxOTQzMTYrMDAwMFx1MDAyNm1lcmNoYW50X2lkPTM0OHBrOWNnZjNiZ3l3MmJcdTAwMjZwdWJsaWNfa2V5PTJuMjQ3ZHY4OWJxOXZtcHIiLCJjb25maWdVcmwiOiJodHRwczovL2FwaS5zYW5kYm94LmJyYWludHJlZWdhdGV3YXkuY29tOjQ0My9tZXJjaGFudHMvMzQ4cGs5Y2dmM2JneXcyYi9jbGllbnRfYXBpL3YxL2NvbmZpZ3VyYXRpb24iLCJjaGFsbGVuZ2VzIjpbXSwiZW52aXJvbm1lbnQiOiJzYW5kYm94IiwiY2xpZW50QXBpVXJsIjoiaHR0cHM6Ly9hcGkuc2FuZGJveC5icmFpbnRyZWVnYXRld2F5LmNvbTo0NDMvbWVyY2hhbnRzLzM0OHBrOWNnZjNiZ3l3MmIvY2xpZW50X2FwaSIsImFzc2V0c1VybCI6Imh0dHBzOi8vYXNzZXRzLmJyYWludHJlZWdhdGV3YXkuY29tIiwiYXV0aFVybCI6Imh0dHBzOi8vYXV0aC52ZW5tby5zYW5kYm94LmJyYWludHJlZWdhdGV3YXkuY29tIiwiYW5hbHl0aWNzIjp7InVybCI6Imh0dHBzOi8vY2xpZW50LWFuYWx5dGljcy5zYW5kYm94LmJyYWludHJlZWdhdGV3YXkuY29tLzM0OHBrOWNnZjNiZ3l3MmIifSwidGhyZWVEU2VjdXJlRW5hYmxlZCI6dHJ1ZSwicGF5cGFsRW5hYmxlZCI6dHJ1ZSwicGF5cGFsIjp7ImRpc3BsYXlOYW1lIjoiQWNtZSBXaWRnZXRzLCBMdGQuIChTYW5kYm94KSIsImNsaWVudElkIjpudWxsLCJwcml2YWN5VXJsIjoiaHR0cDovL2V4YW1wbGUuY29tL3BwIiwidXNlckFncmVlbWVudFVybCI6Imh0dHA6Ly9leGFtcGxlLmNvbS90b3MiLCJiYXNlVXJsIjoiaHR0cHM6Ly9hc3NldHMuYnJhaW50cmVlZ2F0ZXdheS5jb20iLCJhc3NldHNVcmwiOiJodHRwczovL2NoZWNrb3V0LnBheXBhbC5jb20iLCJkaXJlY3RCYXNlVXJsIjpudWxsLCJhbGxvd0h0dHAiOnRydWUsImVudmlyb25tZW50Tm9OZXR3b3JrIjp0cnVlLCJlbnZpcm9ubWVudCI6Im9mZmxpbmUiLCJ1bnZldHRlZE1lcmNoYW50IjpmYWxzZSwiYnJhaW50cmVlQ2xpZW50SWQiOiJtYXN0ZXJjbGllbnQzIiwiYmlsbGluZ0FncmVlbWVudHNFbmFibGVkIjp0cnVlLCJtZXJjaGFudEFjY291bnRJZCI6ImFjbWV3aWRnZXRzbHRkc2FuZGJveCIsImN1cnJlbmN5SXNvQ29kZSI6IlVTRCJ9LCJtZXJjaGFudElkIjoiMzQ4cGs5Y2dmM2JneXcyYiIsInZlbm1vIjoib2ZmIn0=\";\n"
                        + "\n"
                        + "braintree.setup(clientToken, \"dropin\", {\n"
                        + "  container: \"payment-form\"\n"
                        + "});\n"
                        + "</script>", null);
                f.removeAll();
                f.add(cmp);
                f.show();
                        }));
            });

            CommContainer.add(pay);

            f.add(CommContainer);
        }
        if (num > 1) {
            Button pay = new Button("Payer $" + Totale2);
            pay.addActionListener(e -> {

                Dialog.show(
                        "Paiement",
                        FlowLayout.encloseCenterMiddle(new SpanLabel("Choisir votre Methode de Paiement!")),
                        Command.create("Klick&Pay", null, (aActionEvent2) -> {
                            Form hi = new Form("Payement", new BorderLayout());
                            BrowserComponent browser = new BrowserComponent();

                            browser.setURL("http://localhost/tranche.php?MONTANT=" + Totale2 + "&NAME=" + "Sami" + "&EMAIL=" + "sami.asfouri@esprit.tn");
                            f.removeAll();
                            Container con = new Container(new BorderLayout(BorderLayout.CENTER_BEHAVIOR_CENTER));
                            
                            f.add(browser);
                            f.show();
                        }),
                        Command.create("PayPal", null, (aActionEvent2) -> {
                Form hi = new Form("PayPal");
                BrowserComponent cmp = new BrowserComponent();
                cmp.setPage("<form id=\"checkout\" method=\"post\" action=\"/checkout\">\n"
                        + "  <div id=\"payment-form\"></div>\n"
                        + "  <input type=\"submit\" value=\"Pay $" + Totale2 + "\">\n"
                        + "</form>\n"
                        + "\n"
                        + "<script src=\"https://js.braintreegateway.com/js/braintree-2.32.1.min.js\"></script>\n"
                        + "<script>\n"
                        + "// We generated a client token for you so you can test out this code\n"
                        + "// immediately. In a production-ready integration, you will need to\n"
                        + "// generate a client token on your server (see section below).\n"
                        + "var clientToken = \"eyJ2ZXJzaW9uIjoyLCJhdXRob3JpemF0aW9uRmluZ2VycHJpbnQiOiI4OTQyNmY5OTUwNjkzM2EwMWYzZmYyMDlkZWY5NmQ4ODA1ODdhMzZhNTkzYjEzYTc4NWJmODIwMjM0YjdhMTZkfGNyZWF0ZWRfYXQ9MjAxOC0wNS0wMlQwMzo1NzoyNi4wMTMxOTQzMTYrMDAwMFx1MDAyNm1lcmNoYW50X2lkPTM0OHBrOWNnZjNiZ3l3MmJcdTAwMjZwdWJsaWNfa2V5PTJuMjQ3ZHY4OWJxOXZtcHIiLCJjb25maWdVcmwiOiJodHRwczovL2FwaS5zYW5kYm94LmJyYWludHJlZWdhdGV3YXkuY29tOjQ0My9tZXJjaGFudHMvMzQ4cGs5Y2dmM2JneXcyYi9jbGllbnRfYXBpL3YxL2NvbmZpZ3VyYXRpb24iLCJjaGFsbGVuZ2VzIjpbXSwiZW52aXJvbm1lbnQiOiJzYW5kYm94IiwiY2xpZW50QXBpVXJsIjoiaHR0cHM6Ly9hcGkuc2FuZGJveC5icmFpbnRyZWVnYXRld2F5LmNvbTo0NDMvbWVyY2hhbnRzLzM0OHBrOWNnZjNiZ3l3MmIvY2xpZW50X2FwaSIsImFzc2V0c1VybCI6Imh0dHBzOi8vYXNzZXRzLmJyYWludHJlZWdhdGV3YXkuY29tIiwiYXV0aFVybCI6Imh0dHBzOi8vYXV0aC52ZW5tby5zYW5kYm94LmJyYWludHJlZWdhdGV3YXkuY29tIiwiYW5hbHl0aWNzIjp7InVybCI6Imh0dHBzOi8vY2xpZW50LWFuYWx5dGljcy5zYW5kYm94LmJyYWludHJlZWdhdGV3YXkuY29tLzM0OHBrOWNnZjNiZ3l3MmIifSwidGhyZWVEU2VjdXJlRW5hYmxlZCI6dHJ1ZSwicGF5cGFsRW5hYmxlZCI6dHJ1ZSwicGF5cGFsIjp7ImRpc3BsYXlOYW1lIjoiQWNtZSBXaWRnZXRzLCBMdGQuIChTYW5kYm94KSIsImNsaWVudElkIjpudWxsLCJwcml2YWN5VXJsIjoiaHR0cDovL2V4YW1wbGUuY29tL3BwIiwidXNlckFncmVlbWVudFVybCI6Imh0dHA6Ly9leGFtcGxlLmNvbS90b3MiLCJiYXNlVXJsIjoiaHR0cHM6Ly9hc3NldHMuYnJhaW50cmVlZ2F0ZXdheS5jb20iLCJhc3NldHNVcmwiOiJodHRwczovL2NoZWNrb3V0LnBheXBhbC5jb20iLCJkaXJlY3RCYXNlVXJsIjpudWxsLCJhbGxvd0h0dHAiOnRydWUsImVudmlyb25tZW50Tm9OZXR3b3JrIjp0cnVlLCJlbnZpcm9ubWVudCI6Im9mZmxpbmUiLCJ1bnZldHRlZE1lcmNoYW50IjpmYWxzZSwiYnJhaW50cmVlQ2xpZW50SWQiOiJtYXN0ZXJjbGllbnQzIiwiYmlsbGluZ0FncmVlbWVudHNFbmFibGVkIjp0cnVlLCJtZXJjaGFudEFjY291bnRJZCI6ImFjbWV3aWRnZXRzbHRkc2FuZGJveCIsImN1cnJlbmN5SXNvQ29kZSI6IlVTRCJ9LCJtZXJjaGFudElkIjoiMzQ4cGs5Y2dmM2JneXcyYiIsInZlbm1vIjoib2ZmIn0=\";\n"
                        + "\n"
                        + "braintree.setup(clientToken, \"dropin\", {\n"
                        + "  container: \"payment-form\"\n"
                        + "});\n"
                        + "</script>", null);
                f.removeAll();
                f.add(cmp);
                f.show();
                        }));


            });
            f.add(pay);
        }
        f.getToolbar().addCommandToLeftBar("Back", theme.getImage("back-command.png"), e -> {
            MyApplication my = new MyApplication();
            Form f = new Form();
            my.setF(f);
            my.start();
        });
        f.show();
    }

}
