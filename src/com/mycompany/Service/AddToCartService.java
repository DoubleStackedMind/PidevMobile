/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.Service;

import com.codename1.components.ImageViewer;
import com.codename1.ui.Button;
import com.codename1.ui.Container;
import com.codename1.ui.Dialog;
import com.codename1.ui.EncodedImage;
import com.codename1.ui.Form;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.URLImage;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.geom.Dimension;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.plaf.UIManager;
import com.codename1.ui.util.Resources;
import com.mycompany.Entite.Produits;
import com.mycompany.gui.AfficherCommandes;
import com.mycompany.myapp.MyApplication;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Sami
 */
public class AddToCartService {

    Form f = new Form();
    private Resources theme;
    private EncodedImage ei;
    Map<Produits, Integer> myMap = new HashMap();

    public Map<Produits, Integer> getMyMap() {
        return myMap;
    }

    public void setMyMap(Map<Produits, Integer> myMap) {
        this.myMap = myMap;
    }

    public AddToCartService() {

    }

    public void AddToCart(Produits p, int qt) {
        myMap.put(p, qt);
    }

    public void RemoveFromCart(Produits p) {
        if (myMap.containsKey(p)) {
            myMap.remove(p);
        }
    }

    public void DispalyCart() {
        theme = UIManager.initFirstTheme("/theme");
        AddToCartService atcs = new AddToCartService();
        CommandesServices cs = new CommandesServices();
        
        Button Valider = new Button("Valider");
        Valider.addActionListener(e -> {
            if(!myMap.isEmpty()) {
            cs.ValiderPanier(myMap);
            AfficherCommandes a = new AfficherCommandes();
            a.AfficherCommandes();
            } else {
                Dialog.show("Panier", "Panier est vide", "Ok", "Close");
            }
        });
        try {
            ei = EncodedImage.create("/loading.png");
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
        for (Produits p : myMap.keySet()) {

            Container c1 = new Container(new BoxLayout(BoxLayout.Y_AXIS));
            Container c2 = new Container(new BoxLayout(BoxLayout.X_AXIS));
            Container c3 = new Container(new BoxLayout(BoxLayout.Y_AXIS));

            Container IMG = new Container();
            Image img = URLImage.createToStorage(ei, p.getImage(),
                    "http://localhost/PIDEV/web/imagesShop/" + p.getImage(), URLImage.RESIZE_SCALE);
            IMG.setPreferredSize(new Dimension(100, 100));
            img.scale(20, 20);
            ImageViewer iv = new ImageViewer(img);
            IMG.add(iv);
            IMG.setLayout(new BoxLayout(BoxLayout.Y_AXIS));
            iv.getParent().revalidate();
            c2.add(IMG);

            Button remove = new Button("Supprimer");
            remove.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    AddToCartService.this.RemoveFromCart(p);
                    f.removeAll();
                    atcs.DispalyCart();

                }
            });

            Container c4 = new Container();

            Label nomProduits = new Label();
            nomProduits.setText(p.getNom());

            Label PrixProduits = new Label();
            PrixProduits.setText(String.valueOf(p.getPrix()));

            c4.addAll(PrixProduits, remove);
            c1.addAll(nomProduits, c4);
            c2.add(c1);
            c3.add(c2);
            f.add(c3);
        }

        f.getToolbar().addCommandToOverflowMenu("Panier", null, e -> {
            DispalyCart();
        });
        f.add(Valider);
        f.getToolbar().addCommandToLeftBar("Back", theme.getImage("back-command.png"), e -> {
            MyApplication my = new MyApplication();
            Form f = new Form();
            my.setF(f);
            my.start();
        });
        f.show();
        f.getContentPane().addPullToRefresh(new Runnable() {
            @Override
            public void run() {
                f.removeAll();
                atcs.DispalyCart();
            }
        });
    }

}
