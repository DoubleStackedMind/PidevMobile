/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.Service;

import com.codename1.io.CharArrayReader;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.NetworkManager;
import com.mycompany.Entite.Produits;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Sami
 */
public class ProduitsService {

    public List<Produits> consulterProduits() {

        List<Produits> myList = new ArrayList();
        Map<ArrayList<Produits>, ArrayList<String>> myMap = new HashMap();

        ConnectionRequest con = new ConnectionRequest();
        con.setUrl("http://localhost/PIDEv/web/app_dev.php/show/all");
        con.addResponseListener(a -> {
            JSONParser jsonp = new JSONParser();
            try {
                Map<String, Object> prods = jsonp.parseJSON(new CharArrayReader(new String(con.getResponseData()).toCharArray()));
                //System.out.println(tasks);
                List<Map<String, Object>> list = (List<Map<String, Object>>) prods.get("root");
                for (Map<String, Object> obj : list) {
                    Produits p = new Produits();
                    float id = Float.parseFloat(obj.get("idProduit").toString());
                    p.setIdProduit((int) id);
                    p.setNom(obj.get("nom").toString());
                    p.setPrix(Float.parseFloat(obj.get("prix").toString()));
                    p.setCategorie(obj.get("categorie").toString());
                    float qt = Float.parseFloat(obj.get("quantite").toString());
                    p.setQuantite((int) qt);
                    p.setCouleur(obj.get("couleur").toString());
                    p.setDescription(obj.get("description").toString());
                    p.setMarque(obj.get("marque").toString());
                    p.setComposition(obj.get("composition").toString());
                    p.setImage(obj.get("image").toString());
                    myList.add(p);
                }
            } catch (IOException ex) {
                System.out.println(ex.getMessage());
            }

        });
        NetworkManager.getInstance().addToQueueAndWait(con);
        return myList;
    }

    public Produits findProduits(int id) {
        Produits p = new Produits();

        List<Produits> myList = new ArrayList();
        Map<ArrayList<Produits>, ArrayList<String>> myMap = new HashMap();

        ConnectionRequest con = new ConnectionRequest();
        con.setUrl("http://localhost/PIDEv/web/app_dev.php/findP/"+id);
        con.addResponseListener(a -> {
            JSONParser jsonp = new JSONParser();
            try {
                Map<String, Object> prods = jsonp.parseJSON(new CharArrayReader(new String(con.getResponseData()).toCharArray()));
                List<Map<String, Object>> list = (List<Map<String, Object>>) prods.get("root");
                for (Map<String, Object> obj : list) {
                    float idd = Float.parseFloat(obj.get("idProduit").toString());
                    p.setIdProduit((int) idd);
                    p.setNom(obj.get("nom").toString());
                    p.setPrix(Float.parseFloat(obj.get("prix").toString()));
                    p.setCategorie(obj.get("categorie").toString());
                    float qt = Float.parseFloat(obj.get("quantite").toString());
                    p.setQuantite((int) qt);
                    p.setCouleur(obj.get("couleur").toString());
                    p.setDescription(obj.get("description").toString());
                    p.setMarque(obj.get("marque").toString());
                    p.setComposition(obj.get("composition").toString());
                    p.setImage(obj.get("image").toString());
                    break;
                }
            } catch (IOException ex) {
                System.out.println(ex.getMessage());
            }
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
        return p;
    }

}
