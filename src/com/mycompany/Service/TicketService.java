/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.Service;

import com.codename1.io.CharArrayReader;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.NetworkManager;
import com.codename1.processing.Result;
import com.mycompany.Entite.Produits;
import com.mycompany.Entite.Tickets;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Sami
 */
public class TicketService {

    public void ReserverTicket(int idm, int id, int qt) {
        ConnectionRequest con = new ConnectionRequest();
        String Url = "http://localhost/PIDEV/web/app_dev.php/reserver/" + idm + "/" + id + "/" + qt;
        con.setUrl(Url);
        con.addResponseListener((e) -> {
            String str = new String(con.getResponseData());
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
    }

    public List<Tickets> consulterTickets(int idI) {

        List<Tickets> myList = new ArrayList();
        Map<ArrayList<Tickets>, ArrayList<String>> myMap = new HashMap();

        ConnectionRequest con = new ConnectionRequest();
        con.setUrl("http://localhost/PIDEv/web/app_dev.php/show/allM/" + idI);
        con.addResponseListener(a -> {
            JSONParser jsonp = new JSONParser();
            try {
                Map<String, Object> ticks = jsonp.parseJSON(new CharArrayReader(new String(con.getResponseData()).toCharArray()));
                List<Map<String, Object>> list = (List<Map<String, Object>>) ticks.get("root");
                for (Map<String, Object> obj : list) {
                    Tickets p = new Tickets();
                    Result result = Result.fromContent(obj);
                    float id = Float.parseFloat(obj.get("id").toString());
                    p.setId((int) id);
                    p.setIdUser(idI);
                    p.setIdStade(result.getAsInteger("/idUser/id"));
                    p.setPrix(Float.parseFloat(obj.get("prix").toString()));
                    float qt = Float.parseFloat(obj.get("quantite").toString());
                    p.setQuantite((int) qt);
                    p.setIdMatch(result.getAsInteger("/idMatch/idMatch"));
                    p.setIdStade(result.getAsInteger("/idStade/idStade"));
                    myList.add(p);
                }
            } catch (IOException ex) {
            }

        });
        NetworkManager.getInstance().addToQueueAndWait(con);
        return myList;
    }
}
