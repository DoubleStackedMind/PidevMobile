/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.Service;

import com.codename1.io.CharArrayReader;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.NetworkManager;
import com.codename1.processing.Result;
import com.mycompany.Entite.Matches;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Sami
 */
public class SMatchesServices {

    public List<Matches> AfficherMatches() {
        List<Matches> myList = new ArrayList();
        Map<ArrayList<Matches>, ArrayList<String>> myMap = new HashMap();

        ConnectionRequest con = new ConnectionRequest();
        con.setUrl("http://localhost/PIDEv/web/app_dev.php/matchesM");
        con.addResponseListener(a -> {
            JSONParser jsonp = new JSONParser();
            try {
                Map<String, Object> Mats = jsonp.parseJSON(new CharArrayReader(new String(con.getResponseData()).toCharArray()));
                //System.out.println(tasks);
                List<Map<String, Object>> list = (List<Map<String, Object>>) Mats.get("root");
                for (Map<String, Object> obj : list) {
                    Matches p = new Matches();
                    float id = Float.parseFloat(obj.get("idMatch").toString());
                    p.setId_match((int) id);
                    p.setId_groupe(obj.get("idGroupe").toString());
                    float ph = Float.parseFloat(obj.get("phase").toString());
                    p.setPhase((int) ph);
                    //  p.setDate_match(Date.valueOf(obj.get("dateMatch").toString()));
                    //      float ids = Float.parseFloat(obj.get("idStade").toString());
                    //   p.setId_stade((int) ids);
                    p.setLieu_match(obj.get("lieuMatch").toString());
                    String StringA = obj.get("equipea").toString();
                    System.out.println("String A : " + StringA);
                    String EqA = StringA.substring(StringA.indexOf("idequipe=") + 9, StringA.indexOf("idequipe=") + 10);
                    p.setEquipeA(Integer.parseInt(EqA));
                    String StringB = obj.get("equipeb").toString();
                    System.out.println("String B : " + StringB);
                    String EqB = StringB.substring(StringB.indexOf("idequipe=") + 9, StringB.indexOf("idequipe=") + 11);
                    if (EqB.contains(".")) {
                        EqB = EqB.substring(EqB.indexOf(EqB.charAt(0)), EqB.indexOf("."));
                    }
                    p.setEquipeB(Integer.parseInt(EqB));
                    myList.add(p);
                }
            } catch (IOException ex) {
                System.out.println(ex.getMessage());
            }

        });
        NetworkManager.getInstance().addToQueueAndWait(con);
        return myList;
    }

    public Matches findMatche(int x) {
        List<Matches> myList = new ArrayList();
        Map<ArrayList<Matches>, ArrayList<String>> myMap = new HashMap();
        Matches p = new Matches();
        ConnectionRequest con = new ConnectionRequest();
        con.setUrl("http://localhost/PIDEv/web/app_dev.php/findM/" + x);
        con.addResponseListener(a -> {
            JSONParser jsonp = new JSONParser();
            try {
                Map<String, Object> Mats = jsonp.parseJSON(new CharArrayReader(new String(con.getResponseData()).toCharArray()));
                //System.out.println(tasks);
                List<Map<String, Object>> list = (List<Map<String, Object>>) Mats.get("root");
                for (Map<String, Object> obj : list) {
                    Result result = Result.fromContent(obj);
                    float id = Float.parseFloat(obj.get("idMatch").toString());
                    p.setId_match((int) id);
                    p.setId_groupe(obj.get("idGroupe").toString());
                    float ph = Float.parseFloat(obj.get("phase").toString());
                    p.setPhase((int) ph);
                    //  p.setDate_match(Date.valueOf(obj.get("dateMatch").toString()));
                    p.setId_stade(result.getAsInteger("/idStade/idStade"));
                    p.setLieu_match(obj.get("lieuMatch").toString());
                    p.setEquipeA(result.getAsInteger("/equipea/idequipe"));
                    p.setEquipeB(result.getAsInteger("/equipeb/idequipe"));
                    break;
                }
            } catch (IOException ex) {
                System.out.println(ex.getMessage());
            }

        });
        NetworkManager.getInstance().addToQueueAndWait(con);
        return p;
    }

}
