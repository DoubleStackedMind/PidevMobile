/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.Service;

import com.codename1.io.CharArrayReader;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.NetworkManager;
import com.mycompany.Entite.Equipe;
import com.mycompany.Entite.Produits;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Sami
 */
public class SEquipeService {

    public Equipe findEq(int id) {
        List<Equipe> myList = new ArrayList();

        Equipe p = new Equipe();
        Map<ArrayList<Equipe>, ArrayList<String>> myMap = new HashMap();

        ConnectionRequest con = new ConnectionRequest();
        con.setUrl("http://localhost/PIDEV/web/app_dev.php/findE/" + id);
        con.addResponseListener(a -> {
            JSONParser jsonp = new JSONParser();
            try {
                Map<String, Object> eqs = jsonp.parseJSON(new CharArrayReader(new String(con.getResponseData()).toCharArray()));
               
                List<Map<String, Object>> list = (List<Map<String, Object>>) eqs.get("root");
              
                for (String obj : eqs.keySet()) {
                    p.setNom(eqs.get("nom").toString());
                    p.setImage(eqs.get("drapeau").toString());

                    break;
                }
            } catch (IOException ex) {
            }

        });

        NetworkManager.getInstance().addToQueueAndWait(con);
        return p;
    }

}
