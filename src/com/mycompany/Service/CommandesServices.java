/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.Service;

import com.codename1.io.CharArrayReader;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.NetworkManager;
import com.codename1.ui.Form;
import com.mycompany.Entite.Lignedecommande;
import com.mycompany.Entite.Produits;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;

/**
 *
 * @author Sami
 */
public class CommandesServices {

    public void ValiderPanier(Map<Produits, Integer> map) {

        ConnectionRequest con = new ConnectionRequest();
        for (Produits p : map.keySet()) {
            int x = 2;
            System.out.println(p.getIdProduit());
            String Url = "http://localhost/PIDEV/web/app_dev.php/valider/" + p.getIdProduit() + "/" + x + "/" + map.get(p);
            con.setUrl(Url);
            con.addResponseListener((e) -> {
                String str = new String(con.getResponseData());
            });
        }
            NetworkManager.getInstance().addToQueueAndWait(con);
    }

    public void RemoveProduct(int id) {
        ConnectionRequest con = new ConnectionRequest();
        String url = "http://localhost/PIDEV/web/app_dev.php/removep/" + id;
        System.err.println(url);
        con.setUrl(url);
        con.addResponseListener((e) -> {
            String str = new String(con.getResponseData());
            System.out.println(str);
        });
        NetworkManager.getInstance().addToQueueAndWait(con); //appel asynchrone

    }

    public Map<Integer, List<Lignedecommande>> DisplayCommandes() {

        Map<Integer, List<Lignedecommande>> myMap = new HashMap();
        List<Lignedecommande> myList = new ArrayList();

        ConnectionRequest con = new ConnectionRequest();
        con.setUrl("http://localhost/PIDEV/web/app_dev.php/show/allc");

        con.addResponseListener(a -> {
            JSONParser jsonp = new JSONParser();
            try {
                Map<String, Object> ligs = jsonp.parseJSON(new CharArrayReader(new String(con.getResponseData()).toCharArray()));

                List<Map<String, Object>> list = (List<Map<String, Object>>) ligs.get("root");

                for (Map<String, Object> obj : list) {

                    Lignedecommande p = new Lignedecommande();
                    float id = Float.parseFloat(obj.get("idligne").toString());
                    p.setIdLigne((int) id);
                    ObjectMapper mapper = new ObjectMapper();
                    Produits pa = new Produits();
                    String x = obj.get("idproduit").toString();
                    System.out.println(x);
                    String ids = x.substring(x.indexOf("idProduit=") + 10, x.indexOf("idProduit=") + 12);
                    System.out.println(ids);

                    float idP = Float.parseFloat(ids);
                    p.setIdProduit((int) idP);

                    p.setPrix(Float.parseFloat(obj.get("prix").toString()));
                    String idus = obj.get("idUser").toString();
                    System.out.println("USER STRNG : "+idus);
                  String iduss = idus.substring(idus.indexOf("id=")+3,idus.indexOf("id=")+5);
                    System.out.println("ID USER : "+ iduss);
                    float idu = Float.parseFloat(iduss);
                    p.setIdUser((int) idu);
                    String xom = obj.get("commandes").toString();

                    String idxom = xom.substring(xom.indexOf("id=") + 3, xom.indexOf("id=") + 5);
                    float idc = Float.parseFloat(idxom);
                    p.setIdCommande((int) idc);
                    p.setEtat(obj.get("etat").toString());
                    float qt = Float.parseFloat(obj.get("quantite").toString());
                    p.setQuantite((int) qt);
                    myList.add(p);
                }
                for (Lignedecommande l : myList) {
                    if (myMap.containsKey(l.getIdCommande())) {
                        myMap.get(l.getIdCommande()).add(l);
                    } else {
                        List<Lignedecommande> myList2 = new ArrayList();
                        myList2.add(l);
                        myMap.put(l.getIdCommande(), myList2);
                    }
                }

            } catch (IOException ex) {
                System.out.println(ex.getMessage());
            }

        });
        NetworkManager.getInstance().addToQueueAndWait(con);
        return myMap;
    }

}
